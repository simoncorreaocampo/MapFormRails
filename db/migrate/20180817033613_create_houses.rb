class CreateHouses < ActiveRecord::Migration[5.2]
  def change
    create_table :houses do |t|
      t.string :name
      t.string :address
      t.float :longitude
      t.float :latitude
      t.integer :zoom

      t.timestamps
    end
  end
end
