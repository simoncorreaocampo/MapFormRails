class HousesController < ApplicationController

  def index
    @houses = House.all
  end

  def new
    @house = House.new
    @house.zoom = 14
  end

  def create
    @house = House.new params_permit

    if @house.save
      redirect_to houses_path, notice: "House created!"
    else
      render :new
    end
  end

  def edit
    @house = House.find params[:id]
  end

  def update
    @house = House.find params[:id]

    if @house.update params_permit
      redirect_to houses_path, notice: "House updated!"
    else
      render :edit
    end
  end

  def destroy
    @house = House.find params[:id]

    @house.destroy
    redirect_to houses_path, notice: "House deleted!"
  end

  def geocoder
    results = Geocoder.search(params[:address])
    render status: 200, json: {
      latitude: results.first.coordinates[0],
      longitude: results.first.coordinates[1]
    }
  end

  private

  def params_permit
    params.require(:house).permit(:name, :address, :longitude, :latitude, :zoom)
  end

end
